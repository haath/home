# Home folder scripts

A collection of scripts and home folder configurations for my linux accounts. Includes my custom prompt as it is shown in the script below as well as a lot of wrapper [functions and aliases](#usage). Deployed to [get.gmantaos.com](https://get.gmantaos.com/) so that I can automatically fetch and update these files accross all of my VMs.

## Installation

All that is required on a new account is to fetch the script with `wget` or `curl`

```shell
$ wget -q -O - get.gmantaos.com | bash
```

```shell
$ curl -L get.gmantaos.com | bash
```

The script will also automatically reload the `bash` shell so the new configuration should take effect immediately.

The new `.bashrc` comes with a couple of convenience functions.

```shell
# Fetch staple packages like vim and git
$ get_packages
```

```shell
# Update everything from get.gmantaos.com
$ cfg_update
```

## Usage

### Navigation

```shell
# Normal cd also lists the target directory
$ cd /var
backups  cache  lib  local  lock  log  mail  opt  run  spool  swap  tmp  www
```

```shell
# Go up 3 directories
$ up 3
```

### Aliases

```sh
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias ..='up 1'
alias ...='up 2'
alias ....='up 3'
alias back='cd $OLDPWD'
alias svim='sudo vim'
alias ddu='du -sh * && du -sh .'
alias df='df -h'
alias tf='tail -f'
alias py='python3'
```

```sh
# Will use either curl or wget, depending on which is installed
# and send the output to stdout

$ htget example.com
```

### Git

```shell
# Shortcut to git add . && git commit -m 'msg'
$ commit 'msg'
```

```shell
# Shortcut to git pull current branch
$ pull
```

```shell
# Shortcut to git push current branch
$ push
```

```shell
# Shortcut to commit 'msg' and push
$ push 'msg'
```

```shell
# Prettier version of git log
$ git lg

# Prettier version of git --graph
$ git tree

# Enable credential cache for 30min
$ git cache

# Enable permanent credential store
$ git remember

# Run a git pull on every submodule
$ git submods pull origin master

# List local tags
$ git tags

# Fetch remote tags and list them
$ git rtags
```

### Docker

```shell
# Attach to docker container with sh
$ da $CONTAINER
```

```shell
# Follow container log
$ dtail $CONTAINER
```

```shell
# Compact container list
$ dps
NAMES               IMAGE               STATUS              SIZE
gogs                gogs/gogs-rpi       Up 13 hours         45.6MB (virtual 217MB)
pihole              8d9dcd2e43bf        Up 13 hours         3.61MB (virtual 294MB)
```

```shell
# Ports list
$ dports
NAMES               PORTS
gogs                22/tcp, 0.0.0.0:8081->3000/tcp
pihole              0.0.0.0:53->53/tcp, 0.0.0.0:53->53/udp, 0.0.0.0:8080->80/tcp
```

### Misc

```shell
# Run a speedtest without installing anything and without root permissions
# The only dependency is python
$ speedtest
```

```shell
# Extract any archive with the correct command and options
$ extract archive.tar.gz
```

## Apache

The document root may use `RewriteRule` for routing in the future, so the Apache configuration should look like this.

```apache
<VirtualHost *:80>
    # ...
    
    <Directory /var/www/html>
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

Also `mod_headers` and `mod_rewrite` should be enabled.

```shell
$ a2enmod headers
$ a2enmod rewrite

$ systemctl restart apache2
```