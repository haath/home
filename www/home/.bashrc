# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# History
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize


# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# Prompt
check_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

export PROMPT_COMMAND=get_prompt
get_prompt() {

	local ex="$?"
	local BGREEN='\[\033[1;32m\]'
	local GREEN='\[\033[0;32m\]'
	local BRED='\[\033[1;31m\]'
	local RED='\[\033[0;31m\]'
	local BBLUE='\[\033[1;34m\]'
	local BLUE='\[\033[0;34m\]'
	local NORMAL='\[\033[00m\]'
	local YELLOW='\[\033[33m\]'
	local CYAN='\[\033[0;36m\]'
	local BCYAN='\[\033[1;36m\]'
	local BRANCH=$(check_branch)

	local EXIT="${BBLUE}"
	if [ $ex != 0 ]; then
		EXIT="${BRED}"
	fi

	local USER="${BBLUE}┌─[ ${BRED}\u${BBLUE}@${BRED}\h ${BBLUE}]"
	local DIR="${GREEN}\w${YELLOW} \${BRANCH}${GREEN}"
	local PROMPT="${BBLUE}└─${EXIT}$ ${NORMAL}"
	
	PS1="\n${USER} ${DIR}\n${PROMPT}"
}


# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias ..='up 1'
alias ...='up 2'
alias ....='up 3'
alias back='cd $OLDPWD'
alias svim='sudo vim'
alias ddu='du -sh * && du -sh .'
alias df='df -h'
alias tf='tail -f'
alias py='python3'

alias dps="docker ps --format 'table {{.Names}}\t{{.Image}}\t{{.Status}}\t{{.Size}}'"
alias dports="docker ps --format 'table {{.Names}}\t{{.Ports}}'"


function cd() {
    new_directory="$*";
    if [ $# -eq 0 ]; then 
        new_directory=${HOME};
    fi;
    builtin cd "${new_directory}" && ls
}

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


# Functions
up() {
  local d=""
  limit=$1
  for ((i=1 ; i <= limit ; i++))
    do
      d=$d/..
    done
  d=$(echo $d | sed 's/^\///')
  if [ -z "$d" ]; then
    d=..
  fi
  cd $d
}

extract() {
    local x
    ee() { # echo and execute
        echo "$@"
        $1 "$2"
    }
    for x in "$@"; do
        [[ -f $x ]] || continue
        case "$x" in
            *.tar.bz2 | *.tbz2 )    ee "tar xvjf" "$x"  ;;
            *.tar.gz | *.tgz ) ee "tar xvzf" "$x"   ;;
            *.bz2 )             ee "bunzip2" "$x"   ;;
            *.rar )             ee "unrar x" "$x"   ;;
            *.gz )              ee "gunzip" "$x"    ;;
            *.tar )             ee "tar xvf" "$x"   ;;
            *.zip )             ee "unzip" "$x"     ;;
            *.Z )               ee "uncompress" "$x" ;;
            *.7z )              ee "7z x" "$x"      ;;
        esac
    done
}
ziprm () {
	if [ -f $1 ] ; then
		unzip $1
		rm $1
	else
		echo "Need a valid zipfile"
	fi
}

# Do a quick HTTP GET, printing the result to stdout
htget() {
    if type "wget" > /dev/null 2>&1; then
        wget -q -O - $@
    elif type "curl" > /dev/null 2>&1; then
        curl -L $@
    else
        echo "Install either wget or curl"
    fi
}

# Get ownership of file
grab() {
	sudo chown -R ${USER} ${1:-.}
}

# Attach to docker container with sh
da() {
	docker exec -it $1 /bin/sh
}
# Tail container log
dtail() {
	docker logs --tail 100 -f $1
}

# Update configs.
cfg_update() {
    htget https://get.gmantaos.com/ | bash
    source ~/.bashrc
}

# Install packages
get_packages() {
	htget https://get.gmantaos.com/apt | bash
}

# Jumping to usual dirs
goto() {
	if [ $1 = "apch" ]; then
		cd /etc/apache2/sites-available
	elif [ $1 = "nginx" ]; then
		cd /etc/nginx/sites-available
	elif [ $1 = "www" ] || [ $1 = "w" ]; then
		cd /var/www
	elif [ $1 = "bin" ]; then
		cd /usr/local/bin
	else
		cd $1
	fi
}

# Run command until it fails
untilfail() {
    while $@; do :; done
}

# Run command until it succeeds
untilsuccess() {
    while ! $@; do :; done
}

# Speedtest without root and dependencies
speedtest() {
	htget https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -
}

# Git shortcuts
commit() {
	git add .
	git commit -m "$1"
}
pull() {
	git pull --rebase origin "$(branch)"
}
push() {
	if [ -z "$1" ]; then
		git push origin "$(branch)"
	else
		commit "$1"
		git push origin "$(branch)"
	fi
}
gitinit() {
    if [ $# -eq 0 ]; then
        git init
    else
        git init
        git remote add origin $1
    fi
}
rtag() {
    git tag $1
    git push origin $1
}
untag() {
    git tag --delete $1
    git push --delete origin $1
}
retag() {
    untag $1
    rtag $1
}

# Encrypt & Decrypt
encrypt() {
    openssl enc -in $1 -out $2 -md sha512 -pbkdf2 -iter 100000 -aes-256-cbc -pass stdin
}
decrypt() {
    openssl enc -in $1 -out $2 -md sha512 -pbkdf2 -iter 100000 -aes-256-cbc -pass stdin -d
}

# Reload bashrc
reload() {
    source ~/.bashrc
}

# Give access to this machine to all of my public keys
enslave() {
    # Set up the dirs in case it's a new account
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    touch ~/.ssh/authorized_keys
    chmod 600 ~/.ssh/authorized_keys
    chmod go-w ~

    # Fetch public keys
    htget get.gmantaos.com/home/authorized_keys >> ~/.ssh/authorized_keys

    # Remove duplicates
    sort ~/.ssh/authorized_keys | uniq > ~/.ssh/authorized_keys.uniq
    mv ~/.ssh/authorized_keys{.uniq,}
}
