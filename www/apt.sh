#!/bin/sh

apt-get update

apt-get install -y \
    bash \
    git \
    htop \
    tree \
    vim \
    curl \
    pv \
    iperf \
    iftop