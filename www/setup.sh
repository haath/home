#!/bin/sh

echo ""

get_cfg() {
    if type "wget" > /dev/null 2>&1; then
        wget -q -O ~/.$1 https://get.gmantaos.com/home/.$1
    elif type "curl" > /dev/null 2>&1; then
        curl -L -o ~/.$1 https://get.gmantaos.com/home/.$1
    else
        echo "Install either wget or curl"
    fi
}

get_cfg vimrc
get_cfg bashrc
get_cfg gitconfig.alias
get_cfg selected_editor

git config --global --replace-all include.path ".gitconfig.alias"

